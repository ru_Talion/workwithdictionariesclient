﻿using System.Net;

internal class Program
{
    static readonly HttpClient client = new();
    private async static Task Main()
    {
        Console.WriteLine("Enter the domain and port:");
        var domen = Console.ReadLine();

        while (true)
        {
            Console.WriteLine("Input:");
            var userInput = Console.ReadLine();
            try
            {
                using HttpResponseMessage response = await client.GetAsync($"{domen}/api/TakeWords?userInput={userInput}");

                switch (response.StatusCode)
                {
                    case HttpStatusCode.NoContent:
                        {
                            Console.WriteLine("Dictionary is empty.");
                            break;
                        }
                    case HttpStatusCode.BadRequest:
                        {
                            Console.WriteLine("Bad Request.");
                            break;
                        }
                    case HttpStatusCode.OK:
                        {
                            string responseBody = await response.Content.ReadAsStringAsync();
                            Console.WriteLine("\n-----Answer-----");
                            Console.WriteLine(responseBody);
                            Console.WriteLine("----------------\n");
                            break;

                        }
                    default:
                        {
                            Console.WriteLine($"{response.StatusCode}");
                            break;
                        }
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}